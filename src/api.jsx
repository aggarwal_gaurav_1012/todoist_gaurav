import axios from 'axios';

const API_TOKEN = 'your_api_token';
const API_URL = 'https://api.todoist.com/rest/v2';

const axiosInstance = axios.create({
  baseURL: API_URL,
  headers: {
    'Authorization': `Bearer ${API_TOKEN}`
  }
});

export const getTasks = () => axiosInstance.get('/tasks');
export const getProjects = () => axiosInstance.get('/projects');
export const createTask = (data) => axiosInstance.post('/tasks', data);
export const updateTask = (id, data) => axiosInstance.post(`/tasks/${id}`, data);
export const deleteTask = (id) => axiosInstance.delete(`/tasks/${id}`);
export const getSections = (projectId) => axiosInstance.get(`/projects/${projectId}/sections`);
export const createSection = (data) => axiosInstance.post('/sections', data);
export const updateSection = (id, data) => axiosInstance.post(`/sections/${id}`, data);
export const deleteSection = (id) => axiosInstance.delete(`/sections/${id}`);
export const createProject = (data) => axiosInstance.post('/projects', data);
export const updateProject = (id, data) => axiosInstance.post(`/projects/${id}`, data);
export const deleteProject = (id) => axiosInstance.delete(`/projects/${id}`);