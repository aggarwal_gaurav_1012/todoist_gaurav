import React, { useState, useEffect } from 'react';
import { Layout } from 'antd';
import axios from 'axios';
import Sidebar from './components/Sidebar';
import TaskList from './components/TaskList';
import AddTaskModal from './components/AddTaskModal';
import './index.css';

const { Content } = Layout;
const API_TOKEN = '94ea63e84a354be6a248418c79394cef63718e5c';
const API_URL = 'https://api.todoist.com/rest/v2';

const axiosInstance = axios.create({
  baseURL: API_URL,
  headers: {
    Authorization: `Bearer ${API_TOKEN}`,
  },
});

const createTask = (data) => axiosInstance.post('/tasks', data);
const getTasks = () => axiosInstance.get('/tasks');

const App = () => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [tasks, setTasks] = useState([]);

  const refreshTasks = () => {
    getTasks()
      .then((response) => setTasks(response.data))
      .catch((error) => {
        console.error('Error fetching tasks:', error);
      });
  };

  useEffect(() => {
    refreshTasks();
  }, []);

  const showModal = () => {
    setIsModalVisible(true);
  };

  const handleOk = (values) => {
    const data = {
      content: values.taskName,
      description: values.description,
      due_string: values.date ? values.date.format('YYYY-MM-DD') : '',
    };
    createTask(data)
      .then(() => {
        setIsModalVisible(false);
        refreshTasks();
      })
      .catch((error) => {
        console.error('Error creating task:', error);
      });
  };

  const handleCancel = () => {
    setIsModalVisible(false);
  };

  return (
    <Layout style={{ minHeight: '100vh' }}>
      <Sidebar showModal={showModal} />
      <Content style={{ padding: '24px', background: '#fff' }}>
        <TaskList tasks={tasks} />
      </Content>
      <AddTaskModal
        isModalVisible={isModalVisible}
        handleOk={handleOk}
        handleCancel={handleCancel}
      />
    </Layout>
  );
};

export default App;