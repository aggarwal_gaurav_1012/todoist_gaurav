import React from 'react';
import { List } from 'antd';

const TaskList = ({ tasks }) => (
    <List
        itemLayout="horizontal"
        dataSource={tasks}
        renderItem={task => (
            <List.Item>
                <List.Item.Meta
                    title={task.content}
                    description={task.description}
                />
            </List.Item>
        )}
    />
);

export default TaskList;