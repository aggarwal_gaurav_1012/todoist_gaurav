import React, { useState } from 'react';
import { Modal, Form, Input, DatePicker, Button, Dropdown, Menu } from 'antd';
import { CalendarOutlined, InboxOutlined, FlagOutlined, ClockCircleOutlined, EllipsisOutlined } from '@ant-design/icons';

const AddTaskModal = ({ isModalVisible, handleOk, handleCancel }) => {
    const [taskName, setTaskName] = useState('');
    const [description, setDescription] = useState('');
    const [date, setDate] = useState(null);

    const onSubmit = (e) => {
        e.preventDefault();
        handleOk({ taskName, description, date });
    };

    const menu = (
        <Menu>
            <Menu.Item key="0">
                <InboxOutlined /> Inbox
            </Menu.Item>
        </Menu>
    );

    return (
        <Modal
            title={null}
            visible={isModalVisible}
            onCancel={handleCancel}
            footer={null}
            className="add-task-modal"
        >
            <form layout="vertical" className="add-task-form" onSubmit={onSubmit}>
                <Form.Item name="taskName" rules={[{ required: true, message: 'Please input the task name!' }]}>
                    <Input
                        placeholder="Task name"
                        className="task-input"
                        value={taskName}
                        onChange={(e) => setTaskName(e.target.value)}
                    />
                </Form.Item>
                <Form.Item name="description">
                    <Input.TextArea
                        placeholder="Description"
                        className="description-input"
                        value={description}
                        onChange={(e) => setDescription(e.target.value)}
                    />
                </Form.Item>
                <Form.Item name="date">
                    <DatePicker
                        placeholder="Due date"
                        style={{ width: '100%' }}
                        value={date}
                        onChange={(date) => setDate(date)}
                    />
                </Form.Item>
                <div className="task-options">
                    <Button icon={<CalendarOutlined />}>Due date</Button>
                    <Button icon={<FlagOutlined />}>Priority</Button>
                    <Button icon={<ClockCircleOutlined />}>Reminders</Button>
                    <Button icon={<EllipsisOutlined />} />
                </div>
                <div className="task-footer">
                    <Dropdown overlay={menu} trigger={['click']}>
                        <Button>
                            <InboxOutlined /> Inbox <EllipsisOutlined />
                        </Button>
                    </Dropdown>
                    <div className="task-footer-buttons">
                        <Button onClick={handleCancel}>Cancel</Button>
                        <Button type="primary" htmlType="submit" style={{ background: '#fa8072', borderColor: '#fa8072' }}>
                            Add task
                        </Button>
                    </div>
                </div>
            </form>
        </Modal>
    );
};

export default AddTaskModal;