import React, { useState } from 'react';
import { Layout, Menu, Avatar, Button, Typography, Dropdown } from 'antd';
import { InboxOutlined, CalendarOutlined, StarOutlined, HomeOutlined, BookOutlined, CheckCircleOutlined, AppstoreOutlined, PlusCircleFilled, SearchOutlined, BellOutlined, EllipsisOutlined, PlusOutlined } from '@ant-design/icons';
import profileImage from '../images/profile.jpeg';
import AddProjectBox from './AddProjectBox';

const { Sider } = Layout;
const { Title } = Typography;

const Sidebar = ({ showModal }) => {
    const [isAddProjectBoxVisible, setAddProjectBoxVisible] = useState(false);

    const toggleAddProjectBox = (e) => {
        e.stopPropagation();
        setAddProjectBoxVisible(!isAddProjectBoxVisible);
    };

    return (
        <Sider width={250} className="site-layout-sider">
            <div className="profile">
                <Avatar src={profileImage} alt="Profile" style={{ marginRight: '10px' }} />
                <span style={{ fontSize: '15px', fontWeight: 'bold' }}>Gaurav</span>
                <Button style={{ position: 'relative', fontSize: '16px', left: '70px', border: 'none' }}>
                    <BellOutlined />
                </Button>
            </div>
            <Button type="primary" className="add-task-button" style={{ color: '#a81f00' }} onClick={showModal}>
                <PlusCircleFilled style={{ color: '#dc4c3e', fontSize: '22px' }} />
                Add task
            </Button>
            <Menu mode="inline" defaultSelectedKeys={['1']} style={{ borderRight: 0, background: '#fafafa' }}>
                <Menu.Item key="0" icon={<SearchOutlined />}>Search</Menu.Item>
                <Menu.Item key="1" icon={<InboxOutlined />}>Inbox</Menu.Item>
                <Menu.Item key="2" icon={<CalendarOutlined />}>Today</Menu.Item>
                <Menu.Item key="3" icon={<CalendarOutlined />}>Upcoming</Menu.Item>
                <Menu.Item key="4" icon={<AppstoreOutlined />}>Filters & Labels</Menu.Item>
            </Menu>
            <Menu mode="inline" defaultOpenKeys={['favorites', 'projects']} style={{ marginTop: 16, background: '#fafafa' }}>
                <Menu.SubMenu key="favorites" title="Favorites" icon={<StarOutlined />}>
                    <Menu.Item key="5" icon={<HomeOutlined />}>
                        Home
                        <Dropdown overlay={<Menu>
                            <Menu.Item key="5">
                                <EllipsisOutlined />
                            </Menu.Item>
                        </Menu>}>
                            <Button className="menu-ellipsis-button" icon={<EllipsisOutlined />} />
                        </Dropdown>
                    </Menu.Item>
                </Menu.SubMenu>
                <Menu.SubMenu
                    key="projects"
                    title={
                        <span>
                            My Projects
                            <Button className="submenu-plus-button" icon={<PlusOutlined />} onClick={toggleAddProjectBox} />
                        </span>
                    }
                >
                    <Menu.Item key="6" icon={<HomeOutlined />}>
                        Home
                        <Dropdown overlay={<Menu>
                            <Menu.Item key="6">
                                <EllipsisOutlined />
                            </Menu.Item>
                        </Menu>}>
                            <Button className="menu-ellipsis-button" icon={<EllipsisOutlined />} />
                        </Dropdown>
                    </Menu.Item>
                    <Menu.Item key="7" icon={<CheckCircleOutlined />}>
                        My work
                        <Dropdown overlay={<Menu>
                            <Menu.Item key="7">
                                <EllipsisOutlined />
                            </Menu.Item>
                        </Menu>}>
                            <Button className="menu-ellipsis-button" icon={<EllipsisOutlined />} />
                        </Dropdown>
                    </Menu.Item>
                    <Menu.Item key="8" icon={<BookOutlined />}>
                        Education
                        <Dropdown overlay={<Menu>
                            <Menu.Item key="8">
                                <EllipsisOutlined />
                            </Menu.Item>
                        </Menu>}>
                            <Button className="menu-ellipsis-button" icon={<EllipsisOutlined />} />
                        </Dropdown>
                    </Menu.Item>
                </Menu.SubMenu>
            </Menu>
            <AddProjectBox visible={isAddProjectBoxVisible} onClose={toggleAddProjectBox} />
            <div className="quick-start-templates">
                <Title level={5} style={{ marginBottom: '16px', fontSize: '14px', color: '#8c8c8c' }}>Quick-start templates</Title>
                <Menu mode="inline" style={{ borderRight: 0, background: '#fafafa' }}>
                    <Menu.Item key="9">Class Planning</Menu.Item>
                    <Menu.Item key="10">Getting Things Done</Menu.Item>
                    <Menu.Item key="11">Deep Work</Menu.Item>
                    <Menu.Item key="12">Appointments</Menu.Item>
                </Menu>
            </div>
        </Sider>
    );
};

export default Sidebar;