import React from 'react';
import { Button, Card } from 'antd';

const AddProjectBox = ({ visible, onClose }) => {
    if (!visible) return null;

    return (
        <Card style={{ position: 'absolute', top: '50px', right: '10px', zIndex: 1000 }}>
            <Button type="primary" style={{ marginBottom: '10px' }}>Add project</Button>
            <Button type="default">Browse templates</Button>
            <Button type="link" onClick={onClose} style={{ position: 'absolute', top: '5px', right: '5px' }}>Close</Button>
        </Card>
    );
};

export default AddProjectBox;